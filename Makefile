CC	= cc
CFLAGS	= -shared -fPIC 
LIBS	= -ldl
SRC	= wrapper.c
TARGET	= wrapper.so

.PHONY: all clean

all:	
	$(CC) $(CFLAGS) $(LIBS) $(SRC) -o $(TARGET)

test:	all
	@echo "\nls with the wrapper:"
	@LD_PRELOAD=./$(TARGET) ls
	@echo "\nls without the wrapper:"
	@ls

clean:
	rm $(TARGET)
